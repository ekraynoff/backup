import datetime
import time
import glob
import os

print("Started..")

now = datetime.datetime.now()
now_date_str = now.strftime("%Y%m%d")

temp_directory = "/tmp/backup/"
filename = "etc_backup_"+now_date_str+".tar.gz"
path_backup_file = temp_directory+filename

if not os.path.exists(temp_directory):
        os.makedirs(temp_directory)

tar_command_name = "tar czf " + path_backup_file
tar_command_name += " /etc"
tar_command_name += " /var/tools"

os.system(tar_command_name)

backup_storage_path = "/home/evgen/yandex_disk/backup/etc/"
print("Copying...")
cp_command = "cp "+path_backup_file+" "+backup_storage_path
os.system(cp_command)

remove_local_backup = "rm "+path_backup_file
os.system(remove_local_backup)


SIZE_OF_BACKUP_FILES = 3
REMOVE_FILES_MASK = 'etc_backup_*.tar.gz'

files = glob.glob(backup_storage_path + REMOVE_FILES_MASK)
files.sort(key=os.path.getmtime,reverse=True)
if len(files) > SIZE_OF_BACKUP_FILES:
    for file in files[SIZE_OF_BACKUP_FILES:]:
            os.remove(file)

print("Finished..")
##!/usr/bin/env python3