#!/bin/bash
echo "Starting backup"
rm /var/run/rsnapshot.pid
d=$(date +%Y%m%d)
filename="full_system_backup_$d.tar.gz"

rsnapshot hourly
echo "Backup finished"

path_tar_file="/mnt/backup/$filename"
path_hourly="/mnt/backup/hourly.0"
echo "Starting archive"
tar -czvf $path_tar_file $path_hourly
echo "Finished archiving"
echo "Start copying"
cp $path_tar_file /home/evgen/yandex_disk/backup/system/